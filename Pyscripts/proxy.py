import threading
import http.client
import socket
import time
import sys
import pickle
import time
from struct import pack



# Serve to check remote server's availability
def availability_serve(portnumber) :
    HOST = "0.0.0.0"
    srv=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    srv.bind((HOST,portnumber))
    srv.listen(1)
    while True:
        conn, addr =srv.accept()
        data=conn.recv(1024)
        data = data.decode()
        if len(data) == 0: break
        else :
            availability = get_file_from_remote(data,3997)
            if not availability :
                conn.sendall("Not available".encode())
                break
            else :
                conn.sendall("Available".encode())
                break
    conn.close()
    srv.close()

# Getting request from the client
def get_request(reqportnumber):
    HOST="0.0.0.0"
    srv=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    srv.bind((HOST,reqportnumber))
    srv.listen(1)
    conn, addr =srv.accept()
    data=conn.recv(1024)
    data = pickle.loads(data)
    for i in range(len(data)):
        data[i] = data[i].decode()
    conn.close()
    srv.close()
    return data


# Get the file from the remote server
def get_file_from_remote(remoteip,portnumber):
    connection = http.client.HTTPConnection(remoteip, portnumber, timeout=10)
    connection.request("GET","/")
    response = connection.getresponse()
    reply = response.read()
    print("Status: {} and reason: {}".format(response.status, response.reason))
    connection.close()
    return reply

# Sending data to the client
# Vanilla method without manipulation for comparison
def send_data(remoteip, portnumber, data):
    cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cli.connect((remoteip, portnumber))
    length = pack('>Q', len(data))
    cli.sendall(length)
    cli.sendall(data)
    ack = cli.recv(1)
    print("Received Ack")
    cli.close()

def choose_method(instructionlist,data,remoteip,portnumber):
    whichmethod = instructionlist[0]
    if whichmethod == "send_delayed_byte_chunks":
        send_delayed_byte_chunks(int(instructionlist[1]),float(instructionlist[2]),data,remoteip,portnumber)
    elif whichmethod == "method2":
        method2(data)
    else :
        print("Invalid method")

# Manipulating bytes and delay time
def send_delayed_byte_chunks(chunksize, timedelay, data, remoteip, portnumber):
    cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cli.connect((remoteip, portnumber))
    length = pack('>Q', len(data))
    cli.sendall(length)

    if (chunksize > 0) :
        c = 0
        cs = chunksize
        while c < len(data) :
            chunkdata = data[c:c+cs]
            time.sleep(timedelay)
            cli.sendall(chunkdata)
            c = c + cs
        if c > len(data) :
            rest = c - cs
            time.sleep(timedelay)
            lastchunk = data [rest:]
            cli.sendall(lastchunk)
    else :
        cli.sendall(data)

    ack = cli.recv(1)
    print("Received Ack")
    cli.close()
    print("Closed")
    return
def method2(data) :
    print("using method2")
    return

# Main code

availability_serve(3000)
reqlist= get_request(3300)
reply = get_file_from_remote("3.3.3.10",3997)
for i in range(len(reqlist)) :
    j = reqlist[i].split()
    print(j)
    port = 2000 + i
    choose_method(j,reply,"1.1.1.10",port)
    time.sleep(5)
#send_data("1.1.1.10",2000,reply)