import threading
import http.client
import socket
import time
import sys
import pickle
import subprocess
from struct import unpack
import datetime

STATUS = False

#Functions

# Read the request from the file
def read_request(requestfile):
    f = open(requestfile,"r")
    if f.mode == 'r' :
        contents = f.readlines()

    for i in range(len(contents)) :
        contents[i] = contents[i].strip('\n')
    return contents

# Establishing connection to know server's availability
def get_server_availability(remoteip,portnumber):
    cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cli.connect((remoteip, portnumber))
    cmd = str(remoteip)
    cli.sendall(cmd.encode())
    ans = cli.recv(1024)
    ans = ans.decode()
    if ans == "Available" :
        global STATUS
        STATUS = True
    cli.close()

# Turn data into list and serialize
def ready_data_to_send(reqlist):
    reqlist = reqlist[1::]
    for i in range(len(reqlist)) :
        reqlist[i] = reqlist[i].encode()
    result = pickle.dumps(reqlist)
    return result


# Sending request to server
def send_request(remoteip,portnumber,requestlist):
    #create connection
    cli = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    cli.connect((remoteip, portnumber))
    cmd = requestlist
    cli.sendall(cmd)
    #ACK from the server (mainly request input validation)
    ans = cli.recv(1024)
    ans = ans.decode()
    cli.close()



# Get data from the proxy
def get_file(fileportnumber,file):
    flog = open(file,"a+")
    HOST="0.0.0.0"
    srv=socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    srv.bind((HOST,fileportnumber))
    srv.listen(1)
    conn, addr = srv.accept()
    bsize = conn.recv(8)
    (length,) = unpack('>Q', bsize)
    print("length :  " + str(length))
    data = b''
    starttime = datetime.datetime.now()
    while len(data) < length:
        to_read = length - len(data)
        if (to_read < 0 ) :
            break;
        if (to_read > 4096) :
            data = data + conn.recv(4096)
        elif (to_read < 4096) :
            data = data + conn.recv(to_read)
        else :
            break;
    endtime = datetime.datetime.now()
    deltatime = endtime - starttime
    deltatime = deltatime.total_seconds()
    linetoappend = "Received "+ str(len(data)) + " bytes in " + str(deltatime) + " seconds.\n"
    flog.write(linetoappend)
    flog.close()
    print("End of loop")

    assert len(b'\00') == 1
    conn.sendall(b'\00')
    print("Sent Ack")
    conn.close()
    srv.close()
    print("Closed")
    return data




# Main function

requestfile = sys.argv[1]
requestlist = read_request(requestfile)
remoteipaddr = requestlist[0]

get_server_availability(remoteipaddr,3000)

if STATUS :
    serialzedreqlist = ready_data_to_send(requestlist)
    send_request(remoteipaddr,3300,serialzedreqlist)
    for i in range (len(requestlist) - 1):
        port = 2000 + i
        ourdata = get_file(port,"logfile")
        time.sleep(5)
