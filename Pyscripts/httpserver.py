import http.server
import socketserver
import socket

def http_serve() :
    Handler = http.server.SimpleHTTPRequestHandler
    with socketserver.TCPServer(("", 3997), Handler) as httpd:
        print("serving at port", 3997)
        httpd.serve_forever()


http_serve()
